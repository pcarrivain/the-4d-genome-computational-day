# The 4D Genome Computational Day

The spatial folding of genomic DNA plays a crucial role in the regulation of genome activity.
However, the biological mechanisms and the physical principles behind that *Stucture/Function* coupling
remain largely elusive due to the lack of real quantitave frameworks as well as controlled  experiments.
The field of *3D genomics* specifically address these questions and is a booming
field of research: recent advances in genome-wide mapping and imaging techniques
have indeed strikingly improved the resolution at which nuclear genome folding can be analyzed.
It has unveiled a multi-scale spatial compartmentalization with increasing evidences
that such 3D domains may indeed result from and participate to genome function.
In parallel, many computational approaches have been developped to model the spatial
organization and dynamics of DNA/chromatin fibers at different scales and in different organisms (prokaryotes/eukaryotes/virus).
Most of these modeling works are based on *home-made code* or open-source
distributed code that are optimized for the latest available hardware.
Nowadays, the simulation softwares are getting more powerful and sophisticated
but also more numerous, based on different languages and architectures.
Therefore, in that plethora of tools, it may be puzzling for researchers to identify
the appropriate software and hardware, ie that are best customized for the addressed
question and the system to model. In that context, the **Groupement de Recherche ADN**
over the last eight years brings biologists and physicists working together on the
latest hot topics from genome organization. However, the tools that have been
developped are not often distributed to the community. In addition, the increase
of experimental data resolutions tackle the question of the need of higher complexity
models that come along with computational cost. During this **4D genome computational day**,
organized by the CBP, we aim to gather researchers involved in that field.
They will first give an overview of their researchs by focusing on the softwares they used/developped.
The end of the day will be devoted to a open-table discussion on how to distribute and maintain the codes.